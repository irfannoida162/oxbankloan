package com.hcl.oxbank.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.oxbank.entity.Loan;
import com.hcl.oxbank.repository.LoanRepository;

@Service
public class LoanService {

	@Autowired
	LoanRepository loanRepository;
	
	public Loan saveLoanDetails(Loan loan)
	{
		return loanRepository.save(loan);
	}

}
