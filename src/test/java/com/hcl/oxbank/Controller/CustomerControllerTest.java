package com.hcl.oxbank.Controller;

import com.hcl.oxbank.controller.CustomerController;
import com.hcl.oxbank.dto.CustomerDto;
import com.hcl.oxbank.exception.DateException;
import com.hcl.oxbank.service.CustomerService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.mockito.Mockito.verify;

@ExtendWith({MockitoExtension.class})
public class CustomerControllerTest {

    @Mock
    private CustomerService customerService;

    @InjectMocks
    private CustomerController customerController;

    @Test
    public void testCustomerSave() throws DateException {
        CustomerDto customerDto = new CustomerDto();
        customerDto.setDob("02/03/1998");
        String result = customerController.customerSave(customerDto);
        String expected = "you have registered successfully";
        Assertions.assertEquals(expected, result);
        verify(customerService).datasave(customerDto);
    }

    @Test
    public void TestCustomerFutureDate() throws DateException {
        CustomerDto customerDto = new CustomerDto();
        customerDto.setDob("03/07/2025");
        CustomerController customerController = new CustomerController();
        Assertions.assertThrows(DateException.class, () -> {
            customerController.customerSave(customerDto);
        });
    }

    @Test
    public void TestCustomerInvalidDate() throws DateException {
        CustomerDto customerDto = new CustomerDto();
        customerDto.setDob("32/13/2002");
        CustomerController customerController = new CustomerController();
        Assertions.assertThrows(DateException.class, () -> {
            customerController.customerSave(customerDto);
        });
        Mockito.verifyNoInteractions(customerService);
    }
}